page_screen_output(0);
page_output_immediately(1);
# open file
id_in=fopen('modify_me_with_timestamps.txt','rt');
id_ou=fopen('timestamped.txt','wt');
#first line
fprintf(id_ou,'%s\n',[fgetl(id_in) char(9) 'timestamp']);
#the rest of the lines
#next=0;
while ~feof(id_in)
	clin=fgetl(id_in);
	clin_v=sscanf(clin,'%f');
	out_date=datestr(clin_v(2),'yyyy-mm-dd HH:MM:SS.FFF');
	#print to the out file
	fprintf(id_ou,'%s\n',[clin char(9) out_date ' NZST']);
#	if clin_v(1)>next
#		printf('Current ID = %d\n',clin_v(1));
#		next=next+10000;
#	end
end
fclose(id_in)
fclose(id_ou)

