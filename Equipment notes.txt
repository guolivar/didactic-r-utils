GPSs:
Seven Qstarz BT-Q1000X. These units do not have serial numbers but are identified by a university code G1--G7
	Units log: UTC datetime, Local datetime, latitude, longitude, altitude, speed, heading, global coordinates
	Header example: INDEX	TRACK ID	VALID	UTC DATE	UTC TIME	LOCAL DATE	LOCAL TIME	MS	LATITUDE	N/S	LONGITUDE	E/W	ALTITUDE	SPEED	HEADING	G-X	G-Y	G-Z

CO sensors:
Twelve Langan T15n. 
	University code	Serial number
	1	90740	Serial number label is damaged
	2	90706
	4	90741	Unit if faulty 
	5	101270
	6	101271
	7	101272
	8	101273
	9	101274
	CEE01	110991
	CEE02	110982
	CEE03	110983
	CEE04	110993
	Units log: Temperature and carbon monoxide.
	Example header: Date	Time	Temp (�C) LGR S/N: 2382806 c:2	CO (ppm) LGR S/N: 2382806	Host Connected LGR S/N: 2382806	End Of File LGR S/N: 2382806

Kestel 4500 Weather Trackers
Six units available but will only be available at the time of monitoring. No serial numbers known yet. 
	Units log: Datetime, serial time, magnetic north, true north, wind speed, cross wind, head wind, atmospheric temperature, WC?, relative humidity, heat index, dew point, wet bulb temperature, pressure, altitude, DA?
	Example header: 		FORMATTED DATE-TIME	DT	MG	TR	WS	CW	HW	TP	WC	RH	HI	DP	WB	BP	AL	DA
					YYYY-MM-DD HH:MM:SS	s	Mag	TRUE	m/s	m/s	m/s	°C	°C	%	°C	°C	°C	mb	m	m

Particle monitors
	P-trak
	Unknown number or serial numbers. 
					
Possible data collectors: 
	Buya	School of Environment	ENVSCI713 student
	Andrew	School of Environment	ENVSCI713 student
	Luke	School of Environment	ENVSCI713 student
	Heidi	School of Environment	ENVSCI713 student
	Sally	School of Environment	ENVSCI713 student
	Javiera	School of Environment	ENVSCI713 student
	Rishard	School of Environment	ENVSCI713 student
	Mark	School of Environment	ENVSCI713 student
	Gen	School of Environment	ENVSCI713 student
	Yue	School of Environment	ENVSCI713 student
	Alayna	School of Environment	ENVSCI713 student
	Zoe	School of Environment	ENVSCI713 student
	Georgia	School of Environment	Other student
	Shanon	School of Environment	Other student
	Huimin	School of Environment	Other student
	Extra student	School of Environment	Other student
	Jia	School of Environment	Other student
	Daniel	AUT	Project team
	Greg	Heart of the City Inc.	Project team
	Ian	NIWA Auckland	Project team
	Gus	NIWA Auckland	Project team
	Sally (NIWA)	NIWA Auckland	Project team
	Elizabeth	NIWA Auckland	Project team
	Stuart	School of Environment	Project team
	Jenny	School of Environment	Project team
	Kim 	School of Population Health	Project team
	David	School of Population Health	Project team
