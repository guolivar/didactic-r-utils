Summary of Queen Street data
========================================================
```{r calculate,eval=TRUE, echo=FALSE,message=FALSE,prompt=FALSE,results='hide',warning=FALSE}
library("ggplot2", lib.loc="/home/guolivar/R/x86_64-redhat-linux-gnu-library/3.0")
library("lubridate", lib.loc="/home/guolivar/R/x86_64-redhat-linux-gnu-library/3.0")
library("RPostgreSQL", lib.loc="/home/guolivar/R/x86_64-redhat-linux-gnu-library/3.0")
library("openair", lib.loc="/home/guolivar/R/x86_64-redhat-linux-gnu-library/3.0")
p2 <- dbDriver("PostgreSQL")
con2<-dbConnect(p2,user='datauser',password='l3tme1n',host='localhost',dbname='didactic',port=5432)
#####
dataN<-dbGetQuery(con2,paste0("select recordtime at time zone 'NZST' as date, value::numeric as n6 from
                             data.fixedmeasurements
                             where parameterid=408
                              and siteid=2
                              order by date;"))
errors_1<-dbGetQuery(con2,paste0("select recordtime at time zone 'NZST' as date, value from
                             data.fixedmeasurements
                             where parameterid=409
                                 and siteid=2
                                 order by date;"))
dbDisconnect(con2)

#Parse the error message

errors_1$value<-gsub("\\r\\n", "", errors_1$value)
errors_1$value<-gsub(",", ";", errors_1$value)
npoints=length(errors_1$date)
dataNpoints=length(dataN$n6)
errors=NaN*mat.or.vec(npoints,14)
for (i in 1:npoints){
  xtmp<-unlist(strsplit(errors_1$value[i],"[;]"))
  if (length(xtmp)==14){
    errors[i,]<-xtmp
  }
  if (length(xtmp)==10){
    errors[i,]<-c(0,0,0,0,xtmp)
  }
}
# When to ignore a value
# There is an error code
merged_data=merge(x=dataN,y=errors_1,by='date',all.y=TRUE)
merged_data=cbind(merged_data,data.frame(counterok=(errors[,5]=='0000')))
OK_cond=merged_data$n6>0&merged_data$counterok==TRUE

# Calculate the DIURNAL boxplot
diurnal<-boxplot(merged_data$n6[OK_cond]~hour(merged_data$date[OK_cond]),plot=FALSE)
p1=data.frame(Time=0:23)
p1$median=diurnal$stats[3,]
p1$low=diurnal$stats[2,]
p1$high=diurnal$stats[4,]
```
Latest datapoint: 
--------------------------------------------------------
```{r text_diag,echo=FALSE}
tail(dataN,n=1)
tail(errors_1,n=1)
```

Diurnal Cycle
--------------------------------------------------------
```{r diurnal,fig.width=12,fig.height=9,warning=FALSE,message=FALSE,echo=FALSE,eval=TRUE,results='hide',warning=FALSE}
ggplot(p1,aes(Time))+
  geom_line(aes(y=median),colour='black')+
  geom_ribbon(aes(ymin=low,ymax=high),alpha=0.2)+
  geom_point(aes(x=hour(dataN$date[dataNpoints]),y=dataN$n6[dataNpoints],colour='red'),size=10)+
  geom_point(aes(x=hour(merged_data$date[npoints-60]),y=median(merged_data$n6[(npoints-60):npoints],na.rm=TRUE),colour='red'))+
  geom_point(aes(x=hour(merged_data$date[npoints-120]),y=median(merged_data$n6[(npoints-120):(npoints-60)],na.rm=TRUE),colour='red'))+
  geom_point(aes(x=hour(merged_data$date[npoints-180]),y=median(merged_data$n6[(npoints-180):(npoints-120)],na.rm=TRUE),colour='red'))+
  geom_point(aes(x=hour(merged_data$date[npoints-240]),y=median(merged_data$n6[(npoints-240):(npoints-180)],na.rm=TRUE),colour='red'))+
  geom_point(aes(x=hour(merged_data$date[npoints-300]),y=median(merged_data$n6[(npoints-300):(npoints-240)],na.rm=TRUE),colour='red'))+
  ggtitle('Particle Number Concentration in Queen Street')+
  xlab('NZST hour')+
  ylab(expression(N[6] (pt/cc)))+
  scale_colour_discrete(name='',labels=c('Latest datapoint'))
```
Full summary
--------------------------------------------------------
```{r global_summary,fig.width=12, fig.height=9,tidy=TRUE,warning=FALSE,message=FALSE,echo=FALSE,eval=TRUE,warning=FALSE,results='hide',error=FALSE,prompt=FALSE}
timeVariation(merged_data[OK_cond,],pollutant='n6')
```
