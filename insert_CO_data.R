# Push the data to the database ####
library("RPostgreSQL", lib.loc="/home/guolivar/R/x86_64-redhat-linux-gnu-library/3.0")
# Connect to the DB ####
p2 <- dbDriver("PostgreSQL")
con2<-dbConnect(p2,user='adm_penap',password='p3n@p@dm',host='localhost',dbname='didactic',port=5432)
# 3 inserts per record
nrecords=length(T_CO$co_cal1)
for (rec in 1:nrecords){
  # Push Temperature corrected data to flag 1
  st<-dbGetQuery(con2,paste0("insert into data.mobiledata (sensorid, value, sensortime, recordtime, flagid)
                 values(",T_CO$sensorid[rec],",",T_CO$co_corrected[rec],",",T_CO$sensortime[rec],",'",
                             T_CO$timezone[rec]," NZST',1);"))
  # Push First calibration to flag 4
  st<-dbGetQuery(con2,paste0("insert into data.mobiledata (sensorid, value, sensortime, recordtime, flagid)
                 values(",T_CO$sensorid[rec],",",T_CO$co_cal1[rec],",",T_CO$sensortime[rec],",'",
                             T_CO$timezone[rec]," NZST',4);"))
  # Push Second calibration to flag 5
  st<-dbGetQuery(con2,paste0("insert into data.mobiledata (sensorid, value, sensortime, recordtime, flagid)
                 values(",T_CO$sensorid[rec],",",T_CO$co_cal2[rec],",",T_CO$sensortime[rec],",'",
                             T_CO$timezone[rec]," NZST',5);"))
}
st<-dbDisconnect(con2)