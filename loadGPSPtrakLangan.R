#LOAD GPS, PTRAK AND LANGAN DATA
#p <- dbDriver("PostgreSQL")
#con<-dbConnect(p,user='adm_penap',password='p3n@p@dm',host='localhost',dbname='didactic',port=5432)
# a<-"UPDATE data.mobilemeasurements SET geom = ST_GeomFromText('SRID=4326;POINT(' || lon || ' ' || lat || ')') where id>803000;"
#rs<-dbGetQuery(con,a)
#dbDisconnect(con)
source('./load_data.R',echo=F)
source('./load_sensordata_LANGAN.R',echo=F)
source('./load_sensordata_PTRAK.R',echo=F)
source('./load_sensordata_GPS.R',echo=F)