Summary of Queen Street data
========================================================

This page shows how the data from the **Queen Street** site *could* be presented on a live website through a dynamic plot

The **R** code to generate the data for these plots is as follows:

Load libraries:

```r
library("ggplot2", lib.loc = "/home/guolivar/R/x86_64-redhat-linux-gnu-library/3.0")
library("lubridate", lib.loc = "/home/guolivar/R/x86_64-redhat-linux-gnu-library/3.0")
library("RPostgreSQL", lib.loc = "/home/guolivar/R/x86_64-redhat-linux-gnu-library/3.0")
```

```
## Loading required package: DBI
```

```r
library("openair", lib.loc = "/home/guolivar/R/x86_64-redhat-linux-gnu-library/3.0")
```

Connect to the database and query the data for **siteid=2** 

```r
p2 <- dbDriver("PostgreSQL")
con2 <- dbConnect(p2, user = "datauser", password = "l3tme1n", host = "localhost", 
    dbname = "didactic", port = 5432)
##### 
dataN <- dbGetQuery(con2, paste0("select recordtime at time zone 'NZST' as date, value::numeric as n6 from\n                             data.fixedmeasurements\n                             where parameterid=408\n                              and siteid=2;"))
errors_1 <- dbGetQuery(con2, paste0("select recordtime at time zone 'NZST' as date, value from\n                             data.fixedmeasurements\n                             where parameterid=409\n                                 and siteid=2;"))
dbDisconnect(con2)
```

```
## [1] TRUE
```

Parse the error message

```r
errors_1$value <- gsub("\\r\\n", "", errors_1$value)
errors_1$value <- gsub(",", ";", errors_1$value)
npoints = length(dataN$n6)
errors = NaN * mat.or.vec(npoints, 14)
for (i in 1:npoints) {
    xtmp <- unlist(strsplit(errors_1$value[i], "[;]"))
    if (length(xtmp) == 14) {
        errors[i, ] <- xtmp
    }
    if (length(xtmp) == 10) {
        errors[i, ] <- c(0, 0, 0, 0, xtmp)
    }
}
# When to ignore a value There is an error code
no_cond <- (errors[, 5] != "0000")
```


Using **Openair** to generate a summary:

```r
timeVariation(dataN[!no_cond, ], pollutant = "n6")
```

```
## Warning: Detected data with Daylight Saving Time, converting to UTC/GMT
```

![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-4.png) 

