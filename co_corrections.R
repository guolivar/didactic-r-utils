# Correct for temperature ####
# CO temperature correction ((co_temp.co*100-(1.75^((co_temp.temp-20)/10)))/(1+(co_temp.temp-20)*BETA))
T_CO$co_corrected<-NA
BETA = 0.0055 #  T<20
T_CO$co_corrected[T_CO$temp<20]<-((T_CO$co[T_CO$temp<20]*100-(1.75^((T_CO$temp[T_CO$temp<20]-20)/10)))/(1+(T_CO$temp[T_CO$temp<20]-20)*BETA))
BETA = 0.0035 # T>=20
T_CO$co_corrected[T_CO$temp>=20]<-((T_CO$co[T_CO$temp>=20]*100-(1.75^((T_CO$temp[T_CO$temp>=20]-20)/10)))/(1+(T_CO$temp[T_CO$temp>=20]-20)*BETA))
# Inter instrument calibration ####
# Load calibration data
co_cal2 <- read.delim("~/repositories/didactic-r-utils/langan_calibration2.txt")
co_cal1 <- read.delim("~/repositories/didactic-r-utils/langan_calibration1.txt")
# First calibration
T_CO$co_cal1<-NA
for (coid in co_cal1$id){
  curr_idx<-which(T_CO$instrumentid==coid, arr.ind=TRUE)
  m<-co_cal1$m[co_cal1$id==coid]
  c<-co_cal1$c[co_cal1$id==coid]
  T_CO$co_cal1[curr_idx]<-c + m * T_CO$co_corrected[curr_idx]
}
#Second calibration
T_CO$co_cal2<-NA
for (coid in co_cal1$id){
  curr_idx<-which(T_CO$instrumentid==coid, arr.ind=TRUE)
  m<-co_cal2$m[co_cal2$id==coid]
  c<-co_cal2$c[co_cal2$id==coid]
  T_CO$co_cal2[curr_idx]<-c + m * T_CO$co_corrected[curr_idx]
}