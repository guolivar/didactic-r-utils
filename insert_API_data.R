# Push the data to the database ####
library("RPostgreSQL", lib.loc="/home/guolivar/R/x86_64-redhat-linux-gnu-library/3.0")
# Connect to the DB ####
p2 <- dbDriver("PostgreSQL")
con2<-dbConnect(p2,user='adm_penap',password='p3n@p@dm',host='localhost',dbname='didactic',port=5432)
#open the SQL file
inputFile1 <- "/home/guolivar/data/QueenStreet/load_api.txt"
Fcon1  <- file(inputFile1, open = "r")
# Deal with single lines of file for safety of data input.
ix=1
nextprint<-1000
while (length(oneLine <- readLines(Fcon1, n = 1, warn = FALSE)) > 0){
  status<-dbGetQuery(con2,oneLine)
  ix=ix+1
  if (ix>nextprint){
    print(oneLine)
    flush.console()
    nextprint=nextprint+10000
  }
}
close(Fcon1)
st<-dbDisconnect(con2)